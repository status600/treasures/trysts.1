
import pathlib
from os.path import dirname, join, normpath
import sys

name = "womb"

this_directory = str (pathlib.Path (__file__).parent.resolve ())
habitat = str (normpath (join (this_directory, "../..")))
venues = str (normpath (join (habitat, "venues")))
stages = str (normpath (join (habitat, "venues/stages")))
treasure = str (normpath (join (habitat, f"venues/stages/{ name }")))
module_paths = [
	stages
]
db_directory = str (normpath (join (this_directory, "DB")))

def add_paths_to_system (paths):
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))
	

add_paths_to_system ([
	stages
])


#----
#
#
from womb.adventures._ops.on import turn_on
from womb._essence import build_essence
#
#
import biotech
#
#
import rich
#
#
import json
import pathlib
from os.path import dirname, join, normpath
import os
import sys
import subprocess
#
#----




#----
#

if (len (sys.argv) >= 2):
	glob_string = treasure + '/' + sys.argv [1]
	db_directory = False
else:
	glob_string = treasure + '/**/status_*.py'


print ("glob string:", glob_string)
#
#----


print ("changing CWD")

os.chdir (this_directory)
#build_essence ()
#turn_on ()

'''
womb_binary_path = str (normpath (join (this_directory, "../../__dictionary/womb_1")))
subprocess.Popen (
	[ f"{ womb_binary_path }",  "on" ],
	cwd = this_directory
)
'''

bio = biotech.on ({
	"glob_string": glob_string,
	
	"simultaneous": True,
	"simultaneous_capacity": 50,

	"time_limit": 60,

	"module_paths": module_paths,
	"relative_path": this_directory,
	
	"db_directory": db_directory,
	"aggregation_format": 2
})


bio ["off"] ()


def turn_off ():
	womb_binary_path = str (normpath (join (this_directory, "../../__dictionary/womb_1")))
	subprocess.Popen (
		[ f"{ womb_binary_path }",  "off" ],
		cwd = this_directory
	)


import time
time.sleep (2)

rich.print_json (data = bio ["proceeds"] ["alarms"])
rich.print_json (data = bio ["proceeds"] ["stats"])


