


deactivate

#
#	git
#
#
git config --global --add safe.directory /habitat

#
#	tmux
#
#
apt install tmux -y

#
#	obtain and build: bun
#
#
apt install unzip; curl -fsSL https://bun.sh/install | bash; source /root/.bashrc
source /root/.bashrc

#
#	obtain and build: stages
#	
#
python3 /habitat/moves/build_with_uv.py


#
#	obtain and build: poetry ships
#
#
uv pip install poetry ships

. /habitat/.venv/bin/activate

export PATH=$PATH:/habitat/venues/stages/womb/__dictionary

#cd /habitat/venues/stages/trysts/__status/main
#trysts_1 adventures on

#cd /habitat/venues/vue-vite-turbo/ && bun run dev 



cd /habitat/venues/moves