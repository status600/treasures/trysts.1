





from .on import turn_on
from .off import turn_off
from .refresh import refresh
from .status import check_status

from ..demux_hap._controls._clique import demux_hap_clique

import click

def adventures_clique ():
	@click.group ("adventures")
	def group ():
		pass

	
	
	#
	#	vegan on
	#
	@group.command ("on")
	def on ():		
		turn_on ()

	
	@group.command ("off")
	def off ():
		turn_off ()

	@group.command ("refresh")
	def refresh_op ():
		refresh ()

	@group.command ("status")
	def status ():
		check_status ()

	
	group.add_command (demux_hap_clique ())

	return group




#



