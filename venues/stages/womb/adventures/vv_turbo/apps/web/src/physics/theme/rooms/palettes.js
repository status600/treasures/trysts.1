



/*
	1	-	background
	
	text:
		2	-	text
		2.1 -   button text 
	
	button backgrounds:
		3	-	link, button
		3.1 -	shelf move button
		3.2 - 	quantity button
		3.3 - 	banquet button 
		3.4 - 	treasure button 
		3.5 -   router link button
	
	4	-	background 2
	5	-	background
	
	borders:
		6	-	borders
		6.1 -   button border
	
	7	-	shadows
	7.1 -   nav button box shadow
	7.3 -   curtain shadows
	 
	8	-	router link background
	9	-	?
	10	- 	divider lines
	11	-	search background
*/



export const palettes = Object.freeze ({
	"galactic": Object.freeze ({		

	}),
	
	"photos": Object.freeze ({

	})
})