

import { methods } from './methods'

import { rhythm_filter } from 'procedures/dates/rhythm-filter'


export const decor = {
	
	components: {  },
	
	data () {
		const pointer_move_RF = rhythm_filter ({
			every: 100
		});
		
		return {
			pointer_move_RF,
			
			places: [],
			
			cursor_chords: {
				
			},
			
			bounds: {
				"top": {
					"left": "",
					"right": ""
				},
				"bottom": {
					"left": "",
					"right": ""
				}
			}
		}
	},
	
	methods,
	
	mounted () {
		const map = this.draw ()
		this.map = map;
		
		this.map.on ('moveend', this.monitor_map_move);
		this.map.on ('pointermove', this.monitor_pointer_move);

		

		/*
		const layers_1 = [
			new TileLayer({
				source: new OSM(),
			}),
		]
		const map = new Map({
			layers: [raster, vector],

			target: 'map',
			view: new View ({
				center: [0, 0],
				zoom: 1,
			}),
		});
		*/
		
		document.getElementById ('zoom-out').onclick = function () {
			const view = map.getView();
			const zoom = view.getZoom();
			view.setZoom(zoom - 1);
		};

		document.getElementById ('zoom-in').onclick = function () {
			const view = map.getView();
			const zoom = view.getZoom();
			view.setZoom(zoom + 1);
		};
		
	},
	
	beforeUnmount () {
		this.map.un ('moveend', this.monitor_map_move);
		this.map.un ('pointermove', this.monitor_pointer_move);
		
	}
}