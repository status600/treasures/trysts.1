




def crate (the_path):
	from os.path import dirname, join, normpath
	import sys
	import pathlib
	import os

	this_directory = pathlib.Path (__file__).parent.resolve ()

	return str (normpath (join (this_directory, the_path)))


#records = str (normpath (join (this_directory, "stages/womb/[records]")))

'''
	modes:
		"nurture", "business"
'''

essence = {
	"mode": "nurture",
	"alert_level": "caution",
	
	"sanique": {},
	"monetary": {}
}

onsite = "yes"
if (onsite == "yes"):
	essence ["monetary"] ["builtin_node"] = {
		"host": "0.0.0.0",
		"port": "22000",
		"path": crate (
			"stages/womb/[records]/monetary_grove/_data"
		),
		"PID_path": crate (
			"stages/womb/[records]/monetary_grove/the.process_identity_number"
		),
		"logs_path": crate (
			"stages/womb/[records]/monetary_grove/logs/the.logs"
		)
	}
