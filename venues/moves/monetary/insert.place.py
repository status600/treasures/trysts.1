




from os.path import dirname, join, normpath
import sys
import pathlib
field = pathlib.Path (__file__).parent.resolve ()


def add_paths_to_system (paths):
	for path in paths:
		sys.path.insert (0, normpath (join (field, path)))


add_paths_to_system ([
	'../../stages'
])


